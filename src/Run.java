/*
This is a prat of Java-Webserver
Copyright (C) 2014 Michael Sørensen

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Foobar.
If not, see http://www.gnu.org/licenses/.
*/

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Run {

  public static void main(String[] args) throws Exception
  {
    int port = 8880;
    Socket sock;
    
    try(
        ServerSocket webSocket = new ServerSocket(port))
    {
      log("Listening for connection on " + port);
      
      while(true)
      {
        sock = webSocket.accept();
        log("Connection has been made!");
        
        HttpServer client = new HttpServer(port, sock);
        
        Thread thread = new Thread(client);
        thread.start();
      }
    }
    catch (IOException e)
    {
      log("Server error: " + e);
    }
  }
  
  public static void log(String message)
  {
    System.out.println(message);
  }
}