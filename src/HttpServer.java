/*
This is a prat of Java-Webserver
Copyright (C) 2014 Michael Sørensen

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Foobar.
If not, see http://www.gnu.org/licenses/.
*/

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;


public class HttpServer implements Runnable {
  
  private int port;
  private Socket socket;
  private boolean debug = true;
  private String httpRoot = "/Users/michaelsorensen/Dev/KEA/SEM3/Sockets/public";
  private Map<String,String> mimeTypes = new HashMap<String,String>();
  private static final String NO_EXTENSION_MIME_TYPE = "text/plain";
  private static final String UNKNOWN_EXTENSION_MIME_TYPE = "text/plain";

  public HttpServer(int port, Socket socket)
  {
    this.port = port;
    this.socket = socket;
    
    loadExtensionsForMimeTypes();
  }
  
  public void run()
  {
    try {
      initialize();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void initialize() throws Exception
  {
    if(debug)
    {
      System.out.println("New HttpServer object startet!");
    }
    
    //Starts the input from client machine
    InputStream is = socket.getInputStream();
    DataOutputStream dos = new DataOutputStream(socket.getOutputStream());

    // Set up input stream filters.

    BufferedReader br = new BufferedReader(
    new InputStreamReader(is));

    String requestLine = br.readLine();

    System.out.println();  //Echoes request line out to screen
    System.out.println(requestLine);

    //The following obtains the IP address of the incoming connection.

    InetAddress incomingAddress = socket.getInetAddress();
    String ipString= incomingAddress.getHostAddress();
    System.out.println("The incoming address is:   " + ipString);

    //String Tokenizer is used to extract file name from this class.
    StringTokenizer tokens = new StringTokenizer(requestLine);
    
    if(tokens.nextToken().equals("GET"))
    {
      //tokens.nextToken();  // skip over the method, which should be “GET”
      String fileName = tokens.nextToken();
      // Prepend a “.” so that file request is within the current directory.
      fileName = httpRoot + fileName;

      String headerLine = null;
      while ((headerLine = br.readLine()).length() != 0)
      { 
        //While the header still has text, print it
        System.out.println(headerLine);
      }
      
      // Findes filen
      FileInputStream fis = null;
      boolean fileExists = true;
      try {
        fis = new FileInputStream(fileName);
      } catch (FileNotFoundException e) {
        fileExists = false;
      }
      
      System.out.println("Filename: " + fileName);
      
      if (fileExists) {
        dos.writeBytes("HTTP/1.1 200 OK \n");
        dos.writeBytes("Content-Type: " + contentType(fileName) + "\n");
        
      } else {
        dos.writeBytes("HTTP/1.1 404 Not Found \n");
        //dos.writeBytes("Content-Type: text/html \n");
      }
      
      //End of response message construction

      
      System.out.println("MIME Type: " + contentType(fileName));


      // Send a blank line to indicate the end of the header lines.
      //dos.writeBytes("\r\n");

      // Send the entity body.
      if (fileExists) {
        //sendBytes(fis, dos);
        //fis.close();
        
        File file = new File(fileName);
        int fileSize = (int)file.length();
        dos.writeBytes("Content-Length: " + fileSize + "\r\n");
        dos.writeBytes("\r\n");
        
        int howmany = 0;
        int total = 0;
        boolean allSent = false;
        byte[] fileInBytes = new byte[1024];
        if (fileSize <= 0)
        {
          allSent = true;  
        }
        
        while (allSent == false)
        {
          for (int i=0; i<1024; i++) fileInBytes[i] = 'x';
          howmany = fis.read(fileInBytes, 0, 1024);
          //System.out.println("DEBUG:  howmany = " + howmany);
          dos.write(fileInBytes, 0, howmany);
          total = total + howmany;
          if ((total%1048576) == 0) System.out.println("Sent so far: " + (int)(total/1048576) + " MB");
          fileSize = fileSize - howmany;
          //System.out.println("DEBUG:  fileSize = " + fileSize);
          if (fileSize <= 0) allSent = true;
        }
        dos.writeBytes("\r\n");
        
        fis.close();
      }
    }
    else
    {
      System.out.println("Oooops! Something else than GET is detected!");
      dos.writeBytes("HTTP/1.1 500 Internal Error \n");
      dos.writeBytes("Content-Type: text/html \n");
      dos.writeBytes("\n\r");
    }
    
    // Close everything
    dos.close();
    br.close();
    socket.close();

  }

    
    private String contentType(String fileName)
    {
      int lastDot = fileName.lastIndexOf(".");
      String mimeType;
      if (lastDot==-1) {
          mimeType = NO_EXTENSION_MIME_TYPE;
      } else {
          String extension = fileName.substring(lastDot+1);
          mimeType = mimeTypes.get(extension);
          if (mimeType == null) {
              mimeType = UNKNOWN_EXTENSION_MIME_TYPE;
          }
      }
      
      return mimeType;
    }
    
    public void loadExtensionsForMimeTypes()
    {
      mimeTypes.put("htm", "text/html");
      mimeTypes.put("html", "text/html");
      mimeTypes.put("jpg", "image/jpeg");
      mimeTypes.put("jpeg", "image/jpeg");
      mimeTypes.put("gif", "image/gif");
    }
}
